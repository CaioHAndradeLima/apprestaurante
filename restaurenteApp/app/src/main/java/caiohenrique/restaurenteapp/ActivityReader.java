package caiohenrique.restaurenteapp;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

public class ActivityReader extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {
    public static final String EXTRA_READER = "READEREXTRA";
    private static final int ZXING_CAMERA_PERMISSION = 5356;
    private QRCodeReaderView qrCodeReaderView;

    public ActivityReader() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        this.qrCodeReaderView = (QRCodeReaderView)this.findViewById(R.id.qrdecoderview);
        this.qrCodeReaderView.setOnQRCodeReadListener(this);
        this.qrCodeReaderView.setQRDecodingEnabled(true);
        this.qrCodeReaderView.setBackCamera();
    }

    public void onQRCodeRead(String text, PointF[] points) {
        Intent it = new Intent(this, ActivityPedido.class);
        it.putExtra(EXTRA_READER, text);
        this.startActivity(it);
        this.finish();
    }

    protected void onResume() {
        super.onResume();
        this.startCamera();
    }

    private void startCamera() {
        this.runOnUiThread(new Runnable() {
            public void run() {
                if(Build.VERSION.SDK_INT < 23) {
                    ActivityReader.this.qrCodeReaderView.startCamera();
                } else if(ContextCompat.checkSelfPermission(ActivityReader.this, "android.permission.CAMERA") != 0) {
                    ActivityCompat.requestPermissions(ActivityReader.this, new String[]{"android.permission.CAMERA"}, ZXING_CAMERA_PERMISSION);
                } else {
                    ActivityReader.this.qrCodeReaderView.startCamera();
                }

            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if(grantResults.length > 0 && grantResults[0] == 0) {
                    this.startCamera();
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", 0).show();
                }

                return;
            default:
        }
    }

    protected void onPause() {
        super.onPause();
        this.qrCodeReaderView.stopCamera();
    }
}
