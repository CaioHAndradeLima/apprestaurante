package caiohenrique.restaurenteapp.objects;

/**
 * Created by Caio on 26/11/2017.
 */
import android.content.ContentValues;
import android.database.Cursor;
import caiohenrique.restaurenteapp.bancodados.BancoManager;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

public class Usuario {
    private int id;
    private int perm;
    //perm 1 perm avançada
    //perm 2 perm normal
    private String nome;
    private String senha;

    public Usuario() {
    }

    public Usuario(int id) {
        this.setId(id);
    }

    public Usuario(Cursor cursor) {
        this.id = cursor.getInt(0);
        this.perm = cursor.getInt(1);
        this.nome = cursor.getString(2);
        this.senha = cursor.getString(3);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPerm() {
        return this.perm;
    }

    public void setPerm(int perm) {
        this.perm = perm;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return this.senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put("id", Integer.valueOf(this.getId()));
        values.put("perm", Integer.valueOf(this.getPerm()));
        values.put("nome", this.getNome());
        values.put("senha", this.getSenha());
        return values;
    }

    public void saveBD(BancoManager bancoManager) {
        bancoManager.deleteUser();
        bancoManager.inserir(this);
    }

    public String getMessageLoginError() {
        switch(this.perm) {
            case -2:
                return "Existe mais de usuario com mesmo nick no banco de dados";
            case -1:
                return "Acesso negado, revise os campos";
            case 0:
                return "Não houve conexão";
            default:
                return "Ocorreu um erro, contacte ao administrador";
        }
    }

    public Usuario verifyNameAndPassword(BancoManager bancoManager) {
        Usuario user = Requisicao.verifyNameAndPassword(this);
        if(user == null) {
            return null;
        } else {
            if(user.perm > 0) {
                this.saveBD(bancoManager);
            }

            return user;
        }
    }
}
