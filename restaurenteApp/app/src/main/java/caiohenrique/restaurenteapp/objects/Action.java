package caiohenrique.restaurenteapp.objects;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Caio on 27/11/2017.
 */

public class Action {

    private  int id, tipo, idUser;
    //tipo 1 : quando vamos altera o pedido
    private String dat, mensagem;

    public Action(Usuario usuario){
        setIdUser(usuario.getId());
        setDat(getData());
    }
    public int getId() {
        return id;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getTipo() {
        return tipo;
    }
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    public int getIdUser() {
        return idUser;
    }
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    public String getDat() {
        return dat;
    }
    public void setDat(String dat) {
        this.dat = dat;
    }

    public static String getData() {
        Locale locale = new Locale("pt","BR");
        GregorianCalendar calendar = new GregorianCalendar();
        SimpleDateFormat formatador = new SimpleDateFormat("dd' de 'MMMMM' de 'yyyy' - 'HH':'mm'h'",locale);
        return formatador.format(calendar.getTime());
    }

    private Object object;

    public void setObject(Object o) {
        object = o;
    }

    public Object getObject() {
        return object;
    }

}
