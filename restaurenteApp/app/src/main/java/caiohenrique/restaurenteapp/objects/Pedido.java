package caiohenrique.restaurenteapp.objects;

/**
 * Created by Caio on 26/11/2017.
 */

import caiohenrique.restaurenteapp.Interfaces;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.action;

public class Pedido {
    private int id;

    /******
    ** Estados :
     * 0 -- Quando ainda nao esta ativo
     * 1 -- Quando esta ativo
     */
    private int estado;
    private int idcomanda;
    private List<Payment> listPayment;
    private List<Produto> listProduto;
    private Interfaces.OnPostProcessamento onPostProcessamento;
    public Pedido() {
    }

    public Pedido(int idPedido) {
        this.setId(idPedido);
    }

    public List<Payment> getListPayment() {
        return this.listPayment;
    }

    public void setListPayment(List<Payment> listPayment) {
        this.listPayment = listPayment;
    }

    public List<Produto> getListProduto() {
        return this.listProduto;
    }

    public void setListProduto(List<Produto> listProduto) {
        this.listProduto = listProduto;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getIdcomanda() {
        return this.idcomanda;
    }

    public void setIdcomanda(int idcomanda) {
        this.idcomanda = idcomanda;
    }

    public List<Object> getListObjects() {
        List<Object> listObjects = new ArrayList<>();
        for (Payment payment : listPayment) {
            listObjects.add(payment);
        }

        for (Produto produto : listProduto) {
            listObjects.add(produto);
        }

        return listObjects;
    }

    public int getConsumidos(Produto produto) {

        return produto.getConsumidos();
    }

    public void setListenerOfBeckEnd(Interfaces.OnPostProcessamento onPostProcessamento) {
        this.onPostProcessamento = onPostProcessamento;
    }


    public void notifyChangeQuantProduct(Produto produto, Usuario usuario, int quantParaModificar) {
        /*  Deprecated
        Deixado aqui para possiveis implementações de gerenciamento
        Action acao = new Action(usuario);
        acao.setTipo(1);
        acao.setMensagem(new StringBuilder()
                .append(usuario.getNome()).append(" alterou a quantidade do pedido n(")
                .append( Integer.toString( getId())).append(") de R$")
                .append( Integer.toString( getConsumidos(produto)) ).append(" para ")
                .append( Integer.toString( quantParaModificar) ).toString());
         */
        //os params do servidor
        List<Integer> lista = new ArrayList<>();
        //param 1  id do pedido
        //param 2  id do produto
        //param 3  é para modificar em quantos la no servidor?
        lista.add(getId());
        lista.add(produto.getId());
        lista.add(getQuantParaModificar(quantParaModificar,produto));
        if(lista.get(2) < 0)
            return;
            /*****************
            ** Se algo deu errado simplesmente voltamos, o erro ja foi tratado
             */
        //acao.setObject(lista);

        enviarAlteredBeckEnd(lista);

    }

    private void enviarAlteredBeckEnd(final List<Integer> lista) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Pedido p = Requisicao.enviarAlteredePedido(lista);

                if(p == null)
                    onPostProcessamento.onAltered(false,"Não houve conexão");
                else
                    onPostProcessamento.onAltered(true,null);

            }
        }).start();
    }

    public Integer getQuantParaModificar(int qualAtual, Produto produto) {
        //vamos buscar quantos tem pra sabermos a diferenca e aplicarmos no servidor junto a action
        int quantAnt = 0;
        for (Produto produto1 : listProduto) {
            if(produto1.getId() == produto.getId()) {
                //se achar ele retorna a quantidade para modificar
                //se nao, nem retorna
                quantAnt = produto.getConsumidos();
                break;
            }
        }
        int retorno = qualAtual - quantAnt;

        return  validateConsumo(retorno);
    }

    private int validateConsumo(int consumo) {
        if(consumo < 0) {
            onPostProcessamento.onAltered(false, "Nao se pode deixar um consumo negativo");
            return -1;
        }
        return consumo;
    }

    public boolean dontExistsPayment() {
        return (listPayment.size() == 0);
    }

    public boolean paymentAndValorCoincidem() {
        int valueTotal = 0;
        int valuePayment = 0;
        for (Produto produto : this.getListProduto()) {
            valueTotal += (produto.getPreco_venda() * produto.getConsumidos());
        }

        for(Payment payment : listPayment)
            valuePayment += payment.getValor();

        return valuePayment == valueTotal;
    }

    public Pedido finalizar() {
        return Requisicao.finalizarPedido(this);
    }

    public Pedido openPedido() {
        return Requisicao.openPedido(this);
    }
}
