package caiohenrique.restaurenteapp.objects;

import caiohenrique.restaurenteapp.bancodados.Script;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

/**
 * Created by Caio on 26/11/2017.
 */
public class Payment {
    private int id;
    private int valor;
    private int estado;
    private String tipo = "";

    public Payment() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValor() {
        return this.valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public void setValor(String valor) {
        try {
            this.valor = Integer.parseInt(valor.replaceAll(",", ""));
        } catch (Exception e) {
        }

    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMessage() {
        StringBuilder msg = new StringBuilder();
        msg.append("Pago R$ ").append(Script.toPreco(valor)).append(" em ").append(tipo);
        return msg.toString();
    }

    public String validate() {
        if (getTipo().equals(""))
            return "Selecione um tipo!";

        if (getValor() == 0) {
            return "Adicione ao menos um valor";
        }
        return null;
    }

    public Payment notifyInserded() {
        return Requisicao.inserirPayment(this);
    }

    public Payment notifyAltered() {
        return Requisicao.alterarPayment(this);
    }

    public Payment deletePayment() {
        return Requisicao.deletePayment(this);
    }
}
