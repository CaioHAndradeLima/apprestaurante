package caiohenrique.restaurenteapp.objects;

/**
 * Created by Caio on 26/11/2017.
 */

import android.util.Log;

import java.util.List;

import caiohenrique.restaurenteapp.bancodados.Script;
import caiohenrique.restaurenteapp.dialogs.OnDialogProduto.GerenciaProduto;

public class Produto {
    private int id;
    private int quantidade;
    private int preco_custo;
    private int preco_venda;
    private int estado;
    private String nome;
    private int consumidos;

    public Produto() {
    }

    public Produto(int produto) {
        id = produto;
    }

    public void setConsumidos(int consumidos) {
        this.consumidos = consumidos;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantidade() {
        return this.quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPreco_custo() {
        return this.preco_custo;
    }

    public void setPreco_custo(int preco_custo) {
        this.preco_custo = preco_custo;
    }

    public int getPreco_venda() {
        return this.preco_venda;
    }

    public void setPreco_venda(int preco_venda) {
        this.preco_venda = preco_venda;
    }

    public int getEstado() {
        return this.estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setInfos(GerenciaProduto infos) {
        infos.getTvCodigo().setText("Código: " + this.getId());
        infos.getEdtCusto().setText(Script.toPreco(this.getPreco_custo()));
        infos.getEdtVenda().setText(Script.toPreco(this.getPreco_venda()));
        infos.getEdtNome().setText(this.getNome());
        infos.getEdtQuantidade().setText(this.getQuantidade() + "");
        if (this.getEstado() < 3) {
            infos.getSwitchLigado().setChecked(true);
        }

        infos.getButton().setText("Alterar");
    }

    public static Object notifyClickOfButton(GerenciaProduto infos) {
        String validate = validate(infos);
        if (validate != null) {
            return validate;
        } else {
            Produto produto = new Produto(infos.getCodigo());
            produto.gerarInfosProduto(infos);
            return produto;
        }
    }

    private void gerarInfosProduto(GerenciaProduto infos) {
        if (infos.getSwitchLigado().isChecked()) {
            this.setEstado(0);
        } else {
            this.setEstado(3);
        }

        this.setNome(infos.getEdtNome().getText().toString());
        this.setPreco_custo(infos.getEdtCusto().getText().toString());
        this.setPreco_venda(infos.getEdtVenda().getText().toString());
        this.setQuantidade(infos.getEdtQuantidade().getText().toString());
        Log.i("BUG", "QUANTIDADE " + getQuantidade());
    }

    private static String validate(GerenciaProduto infos) {
        if (infos.getEdtNome().toString().replaceAll(" ", "").length() < 2) {
            return new String("Digite o nome do produto");
        }

        if (infos.getEdtQuantidade().getText().length() < 1)
            return new String("Digite a quantidade");

        if (infos.getEdtCusto().getText().length() < 1)
            return new String("Digite o custo");

        return null;

    }

    public void setPreco_custo(String preco_custo) {
        this.preco_custo = Integer.parseInt(preco_custo.replaceAll(",", ""));
    }

    public void setPreco_venda(String preco_custo) {
        this.preco_venda = Integer.parseInt(preco_custo.replaceAll(",", ""));
    }

    public void setQuantidade(String preco_custo) {
        this.quantidade = Integer.parseInt(preco_custo.replaceAll(",", ""));
    }

    public int getConsumidos() {
        return consumidos;
    }

    public void atualizar(Produto produto) {
        this.setQuantidade(produto.getQuantidade());
        this.setNome(produto.getNome());
        this.setEstado(produto.getEstado());
        this.setPreco_venda(produto.getPreco_venda());
        this.setConsumidos(produto.getConsumidos());

    }


    public void getConsumidos(List<Object> mList) {
        for (Object o : mList)
            if (o instanceof Produto) {
                Produto pt = (Produto) o;
                if (getId() == pt.getId()) {
                    setConsumidos(pt.getConsumidos());
                    break;
                }
            }

    }
}
