package caiohenrique.restaurenteapp;

import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import caiohenrique.restaurenteapp.bancodados.BancoManager;
import caiohenrique.restaurenteapp.dialogs.OnDialogAddProduct;
import caiohenrique.restaurenteapp.dialogs.OnDialogPayment;
import caiohenrique.restaurenteapp.objects.Pedido;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.objects.Usuario;
import caiohenrique.restaurenteapp.processa_layout.ListAdapter;
import caiohenrique.restaurenteapp.processa_layout.ListaProdutos;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

public class ActivityPedido extends AppCompatActivity implements Interfaces.Dialog {

    private TextView tvCodigo, tvStatus;
    private ViewGroup container;
    private Pedido pedido;
    private ListaProdutos listaProdutos;
    private ListAdapter listAdapter;
    private Usuario usuario;
    private TextView texteviewPedido;

    boolean isRequisitando = false;
    private FloatingActionButton floatButton;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);


        container = (ViewGroup) findViewById(R.id.pedido);
        usuario = new BancoManager(this).buscarUsuario();


        init();

    }

    private void init() {
        view = getLayoutInflater().inflate(R.layout.layout_loading, getContainer(), false);

        getContainer().addView(view);

        new Thread(new Runnable() {
            @Override
            public void run() {

                pedido = Requisicao.buscarPedido(getIntent().getStringExtra(ActivityReader.EXTRA_READER));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (pedido != null) {

                            notifyPedidoReceivs();
                        } else {
                            makeText("Não houve conexão");
                        }
                    }
                });
            }
        }).start();
    }

    /*******
     ** Chamado toda vez que ocorre a mudanca do pedido
     */
    private void notifyPedidoReceivs() {
        if (tvCodigo == null || view != null) {
            getContainer().removeView(view);
            view = getLayoutInflater().inflate(R.layout.layout_pedido, getContainer(), false);
            getContainer().addView(view);
            recuperarReferencias(view);
        }
        switch (pedido.getEstado()) {
            case 0:
                // nao esta ativo
                pedido = new Pedido();
                pedido.setEstado(0);
                pedido.setIdcomanda(Integer.parseInt(getIntent().getStringExtra(ActivityReader.EXTRA_READER)));
                tvStatus.setText("Não ativo");
                tvCodigo.setText("Ainda não ha um código");
                setPedidoState0();
                break;
            case 1:
                //esta ativo
                tvStatus.setText("Em andamento");
                tvCodigo.setText("Código: " + pedido.getId());
                notifyAtualizacaoPedido();
                setPedidoState1();
                break;
            case 2:
                //se esta finalizado
                tvStatus.setText("Finalizado");
                tvCodigo.setText("Código: " + pedido.getId());
                notifyAtualizacaoPedido();
                setPedidoState2();
                break;
        }

    }

    private void setPedidoState2() {
        listAdapter.notifyPedidoChanged(pedido);
        hideFloatingButton();
        texteviewPedido.setText("Abrir novo pedido");

        texteviewPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRequisitando = true;
                texteviewPedido.setText("Abrindo...");

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Pedido pt = new Pedido();
                        pt.setIdcomanda(pedido.getIdcomanda());
                        pt = pt.openPedido();
                        onPostTryOpenPedido(pt);
                    }
                }).start();

            }
        });
    }

    private void hideFloatingButton() {
        floatButton.hide();
    }

    private void setPedidoState1() {
        texteviewPedido.setText("Encerrar Pedido");
        openFloatingButton();
        texteviewPedido.setOnClickListener(new View.OnClickListener() {

            private void enviarTerminoDoPedido() {
                texteviewPedido.setText("Enviando finalização...");
                isRequisitando = true;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Pedido p = pedido.finalizar();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                isRequisitando = false;
                                if (p != null) {
                                    texteviewPedido.setText("Pedido finalizado!");
                                    pedido.setEstado(2);
                                    notifyPedidoReceivs();
                                } else {
                                    texteviewPedido.setText("Encerrar pedido.");
                                    makeText("Não houve conexão.");
                                }
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onClick(View view) {
                if (pedido.dontExistsPayment()) {
                    makeText("Ainda não existem pagamentos!");
                } else if (!pedido.paymentAndValorCoincidem()) {
                    makeText("O valor total do pedido ainda não foi atingido.");
                } else {
                    enviarTerminoDoPedido();
                }
            }
        });
    }

    private void openFloatingButton() {
        floatButton.show();
        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OnDialogPayment(ActivityPedido.this, pedido.getId()).execute();
            }
        });
    }


    private void setPedidoState0() {
        floatButton.hide();
        texteviewPedido.setText("Abrir pedido");
        texteviewPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        isRequisitando = true;
                        Pedido pt = pedido.openPedido();
                        onPostTryOpenPedido(pt);
                    }
                }).start();
            }

        });
    }

    private void onPostTryOpenPedido(final Pedido pt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                isRequisitando = false;
                if (pt != null) {
                    pedido = pt;
                    notifyPedidoReceivs();
                } else {
                    makeText("Não houve conexão");
                    texteviewPedido.setText("Abrir pedido");
                }
            }
        });
    }

    private void notifyAtualizacaoPedido() {
        List<Object> lista = pedido.getListObjects();
        if (lista.size() > 0) {
            listAdapter = new ListAdapter(lista, (RecyclerView) findViewById(R.id.recyclerview), ActivityPedido.this, pedido);
        } else {
            listaProdutos = new ListaProdutos(this) {
                @Override
                public void onClick(int adapterPosition) {
                    new OnDialogAddProduct(ActivityPedido.this, pedido, listaProdutos.getList().get(adapterPosition)).execute();
                }
            };
            listaProdutos.init();
        }
    }

    private void recuperarReferencias(View view) {
        tvCodigo = (TextView) findViewById(R.id.tvCodigo);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        texteviewPedido = (TextView) findViewById(R.id.textviewpedido);
        floatButton = (FloatingActionButton) findViewById(R.id.floatginbutton);
    }

    private void makeText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public ViewGroup getContainer() {
        return container;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public Pedido getPedido() {
        return pedido;
    }

    /*********
     ** Chamado quando o pedido foi alterado
     */
    public void notifyAlteredList() {

        getContainer().removeView(view);
        init();
    }


    @Override
    public void onBackPressed() {
        if (isRequisitando) {
            return;
        }
        super.onBackPressed();
    }
}
