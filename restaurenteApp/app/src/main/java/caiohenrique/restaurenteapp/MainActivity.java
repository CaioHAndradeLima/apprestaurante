package caiohenrique.restaurenteapp;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import caiohenrique.restaurenteapp.bancodados.BancoManager;
import caiohenrique.restaurenteapp.dialogs.OnDialogLogin;
import caiohenrique.restaurenteapp.dialogs.OnDialogProduto;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.objects.Usuario;
import caiohenrique.restaurenteapp.processa_layout.ListaProdutos;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , Interfaces.Dialog, Interfaces.OnAlteredProduto {

    private Usuario usuario;
    private SearchView search;
    private ListaProdutos listaProdutos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.initDrawer(toolbar);

        NavigationView navigationView = (NavigationView)this.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(this.getUsuario() == null) {
            BancoManager listaProdutos = new BancoManager(this);
            this.usuario = listaProdutos.buscarUsuario();
            if(this.usuario == null) {
                this.openDialogLogin();
            }
        }

        listaProdutos = new ListaProdutos(this);
        listaProdutos.init();
    }

    private void initDrawer(final Toolbar toolbar) {
        final DrawerLayout drawer = (DrawerLayout)this.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void openDialogLogin() {
        OnDialogLogin loginDialog = new OnDialogLogin(this);
        loginDialog.execute();
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void onBackPressed() {
        if(!this.search.isIconified()) {
            this.search.setIconified(true);
        } else {
            DrawerLayout drawer = (DrawerLayout)this.findViewById(R.id.drawer_layout);
            if(drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawers();
            } else {
                super.onBackPressed();
            }

        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager)this.getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
        this.search = searchView;
        this.search.setQueryHint("Pesquise um produto");
        this.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.produtos) {
            (new OnDialogProduto(this)).execute();
        } else if(id == R.id.nav_camera) {
            this.startActivity(ActivityReader.class);
        } else if(id != 2131558590 && id != 2131558591 && id != 2131558592 && id == 2131558593) {

        }

        DrawerLayout drawer = (DrawerLayout)this.findViewById(R.id.drawer_layout);
        drawer.closeDrawers();
        return true;
    }

    private void startActivity(Class<?> clazz) {
        Intent it = new Intent(this, clazz);
        this.startActivity(it);
    }

    public Activity getActivity() {
        return this;
    }

    /****
    ** Quando um produto é alterado...
     */
    @Override
    public void onAltered(Produto produto) {
        listaProdutos.notifyAltered(produto);
    }
}
