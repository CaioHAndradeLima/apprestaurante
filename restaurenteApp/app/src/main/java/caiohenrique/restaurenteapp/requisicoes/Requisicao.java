package caiohenrique.restaurenteapp.requisicoes;

/**
 * Created by Caio on 26/11/2017.
 */

import caiohenrique.restaurenteapp.objects.Action;
import caiohenrique.restaurenteapp.objects.Payment;
import caiohenrique.restaurenteapp.objects.Pedido;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.objects.Usuario;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Requisicao {
    private static final String UrlBasica = "http://192.168.1.150:8080/WebService/rest/";
    private static final String PRODUTO = "produto/";
    private static final String PEDIDO = "pedido/";
    private static final String USUARIO = "usuario/";
    private static final String PAYMENT = "payment/";

    public Requisicao() {
    }

    private static String lerBuilder(BufferedReader reader) throws IOException {
        StringBuilder buffer = new StringBuilder();

        String linha;
        while((linha = reader.readLine()) != null) {
            buffer.append(linha);
        }

        return buffer.toString();
    }

    public static Usuario verifyNameAndPassword(Usuario usuario) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL("http://192.168.1.150:8080/WebService/rest/usuario/login");
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(usuario);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                Usuario var8 = usuario;
                return var8;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            usuario = (Usuario)e.fromJson(lerBuilder(reader), Usuario.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }

        return usuario;
    }

    public static List<Produto> buscaProdutos(int idUltimo) {
        List lista = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        Gson g;
        try {
            String e = "http://192.168.1.150:8080/WebService/rest/produto/list/" + idUltimo;
            URL url = new URL(e);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream != null) {
                reader = new BufferedReader(new InputStreamReader(inputStream));
                g = new Gson();
                Produto[] produto = (Produto[])g.fromJson(lerBuilder(reader), Produto[].class);
                lista = returnListOfGeneric(produto);
                return lista;
            }

            g = null;
        } catch (IOException var18) {
            var18.printStackTrace();
            return lista;
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var17) {
                    var17.printStackTrace();
                }
            }
        }
        return lista;
    }

    private static <T> List<T> returnListOfGeneric(T[] array) {
        ArrayList lista = new ArrayList();
        Object[] var2 = array;
        int var3 = array.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            Object tipo = var2[var4];
            lista.add(tipo);
        }

        return lista;
    }

    public static Produto enviarProduto(Produto produto) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        Produto produtoReturno = null;

        Object var9;
        try {
            Gson e = new Gson();
            URL url = new URL("http://192.168.1.150:8080/WebService/rest/produto/");
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(produto);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream != null) {
                reader = new BufferedReader(new InputStreamReader(inputStream));
                produtoReturno = (Produto)e.fromJson(lerBuilder(reader), Produto.class);
                return produtoReturno;
            }

            var9 = null;
        } catch (IOException var20) {
            var20.printStackTrace();
            return produtoReturno;
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var19) {
                    var19.printStackTrace();
                }
            }

        }

        return (Produto)var9;
    }

    public static Pedido buscarPedido(String idComanda) {
        Pedido pedido = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        Gson g;
        try {
            String e = UrlBasica + PEDIDO + "search/"+ idComanda;
            URL url = new URL(e);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream != null) {
                reader = new BufferedReader(new InputStreamReader(inputStream));
                g = new Gson();
                pedido = g.fromJson(lerBuilder(reader), Pedido.class);
                return pedido;
            }

            g = null;
        } catch (IOException var18) {
            var18.printStackTrace();
            return pedido;
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var17) {
                    var17.printStackTrace();
                }
            }
        }
        return pedido;

    }

    public static Pedido enviarAlteredePedido(List<Integer> lista) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        Pedido pedido = null;
        try {
            Gson e = new Gson();
                URL url = new URL(UrlBasica + PEDIDO);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("PUT");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(lista);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pedido = e.fromJson(lerBuilder(reader), Pedido.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }

        return pedido;
    }

    public static Produto alterarProduto(Produto produto) {
        Produto pt = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL(UrlBasica + PRODUTO);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("PUT");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(produto);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pt = e.fromJson(lerBuilder(reader), Produto.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }
        return pt;
    }

    public static Payment inserirPayment(Payment payment) {
        Payment pt = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL(UrlBasica + PAYMENT);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(payment);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pt = e.fromJson(lerBuilder(reader), Payment.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }
        return pt;
    }


    public static Payment alterarPayment(Payment payment) {
        Payment pt = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL(UrlBasica + PAYMENT);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("PUT");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(payment);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pt = e.fromJson(lerBuilder(reader), Payment.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }
        return pt;
    }


    public static Payment deletePayment(Payment payment) {
        Payment pt = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL(UrlBasica + PAYMENT);
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("DELETE");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(payment);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pt = e.fromJson(lerBuilder(reader), Payment.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }
        return pt;
    }

    public static Pedido finalizarPedido(Pedido pedido) {
        Pedido pt = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL(UrlBasica + PEDIDO + "finalizar");
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(pedido);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pt = e.fromJson(lerBuilder(reader), Pedido.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }
        return pt;
    }

    public static Pedido openPedido(Pedido pedido) {
        Pedido pt = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            Gson e = new Gson();
            URL url = new URL(UrlBasica + PEDIDO );
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setChunkedStreamingMode(0);
            PrintStream printStream = new PrintStream(urlConnection.getOutputStream());
            String json = e.toJson(pedido);
            printStream.println(json);
            printStream.flush();
            printStream.close();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            if(inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            pt = e.fromJson(lerBuilder(reader), Pedido.class);
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }

            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException var18) {
                    var18.printStackTrace();
                }
            }

        }
        return pt;
    }
}
