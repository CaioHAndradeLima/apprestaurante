package caiohenrique.restaurenteapp.processa_layout;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import caiohenrique.restaurenteapp.R;

/**
 * Created by Caio on 26/11/2017.
 */

class HolderLoading extends RecyclerView.ViewHolder {
    ProgressBar progress;

    public HolderLoading(View itemView) {
        super(itemView);
        this.progress = (ProgressBar)itemView.findViewById(R.id.progressbar);
    }

    public void onBindLoading(int position) {
        this.progress.setVisibility(View.VISIBLE);
    }
}
