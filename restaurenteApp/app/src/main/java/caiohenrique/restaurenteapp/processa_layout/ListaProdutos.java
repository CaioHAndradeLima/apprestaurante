package caiohenrique.restaurenteapp.processa_layout;

/**
 * Created by Caio on 26/11/2017.
 */

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

import caiohenrique.restaurenteapp.Interfaces;
import caiohenrique.restaurenteapp.Interfaces.Dialog;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.dialogs.OnDialogProduto;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

import java.util.BitSet;
import java.util.List;

public class ListaProdutos implements Interfaces.onClickHolder {
    private RecyclerView recyclerView;
    private Dialog methods;
    private List<Produto> mList = null;
    private ListaProdutos.Adaptador adapter;

    public ListaProdutos(Dialog methods) {
        this.recyclerView = (RecyclerView) methods.getActivity().findViewById(R.id.recyclerview);
        this.methods = methods;
    }

    public RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    public void init() {
        this.adapter = new ListaProdutos.Adaptador();
        if (this.methods.getActivity().getResources().getBoolean(R.bool.isTablet)) {
            this.recyclerView.setLayoutManager(new GridLayoutManager(this.methods.getActivity(), 2));
        } else {
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this.methods.getActivity()));
        }

        this.recyclerView.setAdapter(this.adapter);
        this.realizaRequisicao();
    }

    private void realizaRequisicao() {
        (new Thread(new Runnable() {
            public void run() {
                List lista = Requisicao.buscaProdutos(0);
                if (lista != null && lista.size() > 0) {
                    ListaProdutos.this.mList = lista;
                    ListaProdutos.this.methods.getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            ListaProdutos.this.adapter.notifyDataSetChanged();
                        }
                    });
                }

            }
        })).start();
    }

    @Override
    public void onClick(int adapterPosition) {
        new OnDialogProduto(ListaProdutos.this.methods, ListaProdutos.this.mList.get(adapterPosition)).execute();
    }

    public void notifyAltered(Produto produto) {
        for(int i = 0 ; i < mList.size() ; i++)
            if(mList.get(i).getId() == produto.getId()) {
                mList.get(i).atualizar(produto);
                adapter.notifyItemChanged(i);
                break;
            }


    }

    public List<Produto> getList() {
        return mList;
    }


    public class Adaptador extends Adapter<ViewHolder> {
        public Adaptador() {
        }

        @Override
        public int getItemViewType(int position) {
            return ListaProdutos.this.mList == null ? 0 : 1;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case 0:
                    return new HolderLoading(ListaProdutos.this.methods.getLayoutInflater().inflate(R.layout.layout_loading, parent, false));
                case 1:
                    return new HolderProduto(ListaProdutos.this.methods.getLayoutInflater().inflate(R.layout.layout_lista_produto, parent, false), ListaProdutos.this
                    );
                default:
                    throw new IllegalArgumentException("IMPLEMENTS THIS VIEW TYPE");
            }
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (ListaProdutos.this.mList == null) {
                //((HolderLoading)holder).onBindLoading(position);
            } else {
                if (holder instanceof HolderProduto)
                    ((HolderProduto) holder).bindViewHolder(position, mList.get(position));
            }
        }

        public int getItemCount() {
            return ListaProdutos.this.mList == null ? 2 : ListaProdutos.this.mList.size();
        }


    }

}
