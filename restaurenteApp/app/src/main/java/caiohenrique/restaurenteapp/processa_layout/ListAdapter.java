package caiohenrique.restaurenteapp.processa_layout;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import caiohenrique.restaurenteapp.ActivityPedido;
import caiohenrique.restaurenteapp.Interfaces;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.bancodados.Script;
import caiohenrique.restaurenteapp.dialogs.OnDialogAddProduct;
import caiohenrique.restaurenteapp.objects.Payment;
import caiohenrique.restaurenteapp.objects.Pedido;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

/**
 * Created by Caio on 26/11/2017.
 * <p>
 * Lista o recycler view da tela ActivityPedido
 */

public class ListAdapter implements Interfaces.onClickHolder {

    private List<Object> mList = null;
    private Interfaces.Dialog methods;

    private List<Produto> listProdutos = new ArrayList<>();
    private Adapter adapter;
    private Pedido pedido;

    public ListAdapter(List<Object> list, RecyclerView recycler, Interfaces.Dialog methods, Pedido pedido) {
        mList = list;
        this.methods = methods;
        recycler.setLayoutManager(new LinearLayoutManager(methods.getActivity()));
        adapter = new Adapter();
        recycler.setAdapter(adapter);

        if (((ActivityPedido) methods).getPedido().getEstado() == 1)
            init();

        this.pedido = pedido;

    }

    private void init() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                //tempo para montar layout
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                final List<Produto> list = Requisicao.buscaProdutos(0);
                //apenas se esta em estado de venda caira aqui
                if (list != null) {
                    methods.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (Produto produto : list) {
                                adapter.addListItem(produto);
                            }
                            list.clear();
                        }
                    });

                }
            }
        }).start();


    }

    private int getSizeOfListProdutos(int position) {
        return position - mList.size() - 1;
    }

    public void notifyPedidoChanged(Pedido pedido) {
        this.pedido = pedido;
    }

    public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case 0:
                    return new HolderPayment(methods.getLayoutInflater().inflate(R.layout.layout_payment_sample, parent, false));
                case 1:
                    return new HolderProduct(methods.getLayoutInflater().inflate(R.layout.layout_lista_produto_payment, parent, false));
                case 2:
                    return new HolderDivisor(methods.getLayoutInflater().inflate(R.layout.layout_divisor, parent, false));
                case 3:
                    HolderProduto holder = new HolderProduto(methods.getLayoutInflater().inflate(R.layout.layout_lista_produto, parent, false), ListAdapter.this);
                    return holder;
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            Log.i("LISTA","POSITION " + position);
            switch (getItemViewType(position)){
                case 0:

                    ((HolderPayment) holder).bindViewHolder(position);
                    break;
                case 1:

                    ((HolderProduto) holder).bindViewHolder(position, (Produto) mList.get(position));
                    break;
                case 2:
                    //faz nada
                    break;
                case 3:
                    ((HolderProduto) holder).bindViewHolder(position,listProdutos.get( getSizeOfListProdutos(position) ));
                    break;
            }


        }


        @Override
        public int getItemCount() {
            //se esta em estado de venda
            if (((ActivityPedido) methods.getActivity()).getPedido().getEstado() == 1)
                return mList.size() + 1 + listProdutos.size();

            return mList.size();
        }

        private void addListItem(Produto produto) {
            Log.i("LISTA", "addListItem " + (listProdutos.size() + mList.size() + 1));
            listProdutos.add(produto);
            notifyItemInserted(listProdutos.size() + mList.size() + 1);
        }

        @Override
        public int getItemViewType(int position) {
            if (mList.size() > position)
                if (mList.get(position) instanceof Payment) {
                    return 0;
                } else if (mList.get(position) instanceof Produto) {
                    return 1;
                }
            if (mList.size() == position) {
                return 2;
            }
            Log.i("LISTA", "getItemViewType get produto " + (position - mList.size()));
            //if(listProdutos.get(position - mList.size()) != null) {
            return 3;
            //}

            //a ideia é nunca chegar aqui
            //throw new IllegalAccessError("Erro");
        }
    }

    public void clear() {
        if(listProdutos != null) {
            listProdutos.clear();
        }

    }

    public class HolderProduct extends HolderProduto implements Interfaces.onClickHolder {

        TextView tvConsumidos, tvTotal;

        public HolderProduct(View itemView) {
            super(itemView, null);
            setOnClick(this);
        }

        @Override
        public void recuperarReferencias() {
            super.recuperarReferencias();
            tvConsumidos = itemView.findViewById(R.id.textview_consumidos_);
            tvTotal = itemView.findViewById(R.id.textview_total_);
        }

        @Override
        public void bindViewHolder(int position, Produto produto) {
            super.bindViewHolder(position, produto);
            tvConsumidos.setText(produto.getConsumidos() + "");
            tvTotal.setText(Script.toPreco(produto.getConsumidos() * produto.getPreco_venda()));
        }

        @Override
        public void onClick(int position) {
            openAlteredProduct(position);
        }
    }


    @Override
    public void onClick(int position) {

        openAlteredProduct(position);


    }

    private void openAlteredProduct(int adapterPosition) {
        if (mList.size() == adapterPosition) return;

        if(pedido.getEstado() != 1) {
            makeText("Não é possível adicionar produtos neste pedido!");
            return;
        }

        if (adapterPosition > mList.size()) {

            Produto produto = listProdutos.get(getSizeOfListProdutos(adapterPosition));
            produto.getConsumidos(mList);
            new OnDialogAddProduct(methods, pedido, listProdutos.get(getSizeOfListProdutos(adapterPosition))).execute();

        } else
            new OnDialogAddProduct(methods, pedido, (Produto) mList.get(adapterPosition)).execute();
    }

    private void makeText(String msg) {
        Toast.makeText(methods.getActivity(),msg,Toast.LENGTH_LONG).show();
    }
    public class HolderPayment extends RecyclerView.ViewHolder implements Interfaces.onClickHolder {

        TextView tvPayment;

        public HolderPayment(View itemView) {
            super(itemView);
            recuperarReferencias();
        }

        public void recuperarReferencias() {
            tvPayment = itemView.findViewById(R.id.textviewdinheiro_);
        }

        public void bindViewHolder(int position) {
            tvPayment.setText((((Payment)mList.get(position)).getMessage()));
        }

        @Override
        public void onClick(int position) {

        }
    }

    private class HolderDivisor extends RecyclerView.ViewHolder {

        public HolderDivisor(View itemView) {
            super(itemView);
        }
    }

}
