package caiohenrique.restaurenteapp.processa_layout;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import caiohenrique.restaurenteapp.Interfaces;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.bancodados.Script;
import caiohenrique.restaurenteapp.dialogs.OnDialogProduto;
import caiohenrique.restaurenteapp.objects.Produto;

/**
 * Created by Caio on 26/11/2017.
 */

public class HolderProduto extends RecyclerView.ViewHolder implements View.OnClickListener, Interfaces.Holders {
    private TextView codigo;
    private TextView nome;
    private TextView quantidade;
    private TextView valor;
    private Interfaces.onClickHolder onClicklabe;

    public HolderProduto(View itemView, Interfaces.onClickHolder onClick ) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.onClicklabe = onClick;
        this.recuperarReferencias();
    }

    @Override
    public void recuperarReferencias() {
        this.codigo = (TextView)this.itemView.findViewById(R.id.textview_codigo_);
        this.nome = (TextView)this.itemView.findViewById(R.id.textview_nome_);
        this.quantidade = (TextView)this.itemView.findViewById(R.id.textview_quantidade_);
        this.valor = (TextView)this.itemView.findViewById(R.id.textview_valor_);
    }

    void setOnClick(Interfaces.onClickHolder on){
        onClicklabe = on;

    }

    @Override
    public void bindViewHolder(int position, Produto produto) {
        this.codigo.setText(produto.getId() + "");
        this.nome.setText(produto.getNome());
        this.quantidade.setText(produto.getQuantidade() + "");
        this.valor.setText(Script.toPreco(produto.getPreco_venda()));

    }


    public void onClick(View view) {

        onClicklabe.onClick(getAdapterPosition());

    }
}