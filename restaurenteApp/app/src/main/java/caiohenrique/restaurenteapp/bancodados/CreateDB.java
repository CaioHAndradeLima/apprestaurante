package caiohenrique.restaurenteapp.bancodados;

/**
 * Created by Caio on 26/11/2017.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import caiohenrique.restaurenteapp.bancodados.Script;

public class CreateDB extends SQLiteOpenHelper {
    private static final String NOME_BANCO = "baserestaurante";
    private static final int VERSAO = 1;

    public CreateDB(Context context) {
        super(context, "baserestaurante", (CursorFactory)null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.getTableUser());
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists usuario");
    }

    private String getTableUser() {
        Script pt = new Script();
        pt.createTable("usuario");
        pt.addAtributo("id", " int");
        pt.addAtributo("perm", " int");
        pt.addAtributo("nome", " text");
        pt.addAtributo("senha", " text");
        return pt.build();
    }
}
