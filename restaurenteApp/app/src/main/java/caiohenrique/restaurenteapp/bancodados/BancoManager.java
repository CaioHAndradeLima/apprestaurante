package caiohenrique.restaurenteapp.bancodados;

/**
 * Created by Caio on 26/11/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import caiohenrique.restaurenteapp.bancodados.CreateDB;
import caiohenrique.restaurenteapp.objects.Usuario;

public class BancoManager {
    private SQLiteDatabase db;
    private CreateDB banco;

    public BancoManager(Context c) {
        this.banco = new CreateDB(c);
    }

    public void inserir(Usuario usuario) {
        this.db = this.banco.getWritableDatabase();
        ContentValues values = usuario.getContentValues();
        this.db.insertOrThrow("usuario", (String)null, values);
        this.db.close();
    }

    public Usuario buscarUsuario() {
        this.db = this.banco.getReadableDatabase();
        Usuario usuario = null;
        Cursor cursor = this.db.query("usuario", (String[])null, (String)null, (String[])null, (String)null, (String)null, (String)null);
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            usuario = new Usuario(cursor);
        }

        cursor.close();
        return usuario;
    }

    public void deleteUser() {
        this.db = this.banco.getWritableDatabase();
        this.db.delete("usuario", (String)null, (String[])null);
        this.db.close();
    }
}
