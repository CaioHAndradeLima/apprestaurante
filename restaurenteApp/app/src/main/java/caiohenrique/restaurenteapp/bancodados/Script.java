package caiohenrique.restaurenteapp.bancodados;

/**
 * Created by Caio on 26/11/2017.
 */

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

public class Script {
    public static final String NAMETABLE = "usuario";
    public static final String ID_0 = "id";
    public static final String PERM_1 = "perm";
    public static final String NOME_2 = "nome";
    public static final String SENHA_3 = "senha";
    public String script_tabelas;
    private boolean verif = false;

    public Script() {
    }

    public void createTable(String NomeDaTabela) {
        this.script_tabelas = "CREATE TABLE " + NomeDaTabela + " ( ";
    }

    public void addAtributo(String name, String tipagem) {
        if(this.script_tabelas.equals("")) {
            throw new IllegalArgumentException("ILEGAL ARGUMENT EXCEPTION BEFORE CREATETABLE PLIS");
        } else if(!this.verif) {
            this.script_tabelas = this.script_tabelas + " \n " + name + " " + tipagem + " ";
            this.verif = true;
        } else {
            this.script_tabelas = this.script_tabelas + ", \n " + name + " " + tipagem + " ";
        }
    }

    public String build() {
        return this.script_tabelas = this.script_tabelas + " );";
    }

    public static void setListener(final EditText edit) {
        edit.addTextChangedListener(new TextWatcher() {
            boolean foiModified = false;
            int ultimoSize = 0;

            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            private void setText(String text) {
                edit.setText(text);
                edit.setSelection(text.length());
            }

            public void afterTextChanged(Editable editable) {
                this.foiModified = !this.foiModified;
                if(this.foiModified) {
                    String sequence = editable.toString();
                    int sizeAtual = sequence.length();
                    if(sizeAtual > 2) {
                        Log.i("ALTERANDO", ">2");
                        sequence = sequence.replaceAll(" ", "");
                        sequence = sequence.replaceAll(",", "");
                        if(sequence.length() > 2) {
                            this.ultimoSize = sizeAtual = sequence.length();
                            this.setText(sequence.substring(0, sizeAtual - 2) + "," + sequence.substring(sizeAtual - 2, sizeAtual));
                        } else {
                            this.setText(sequence);
                        }
                    } else {
                        this.setText(sequence.replaceAll(",", ""));
                    }
                }

                this.ultimoSize = edit.getText().toString().length();
            }
        });
    }

    public static String toPreco(int preco) {
        String valor = Integer.toString(preco);
        int sizeAtual = valor.length();
        return sizeAtual > 2?valor.substring(0, sizeAtual - 2) + "," + valor.substring(sizeAtual - 2, sizeAtual):"0," + preco;
    }
}
