package caiohenrique.restaurenteapp.dialogs;

/**
 * Created by Caio on 26/11/2017.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import caiohenrique.restaurenteapp.Interfaces;

abstract class OnDialogBasic  implements Interfaces.CycleLifeDialog, Interfaces.Dialog {
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;
    Interfaces.Dialog methods;

    public OnDialogBasic(Interfaces.Dialog methods) {
        this.methods = methods;
    }

    public Activity getActivity() {
        return this.methods.getActivity();
    }

    public LayoutInflater getLayoutInflater() {
        return this.methods.getLayoutInflater();
    }


    public final View createView(int idView) {
        return this.getLayoutInflater().inflate(idView, null);
    }

    @Override
    public android.app.AlertDialog.Builder onCreateDialog(android.app.AlertDialog.Builder builder) {
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode == 4 && event.getAction() == 1 && !event.isCanceled()) {
                    if(!OnDialogBasic.this.onBackPressed()) {
                        OnDialogBasic.this.getDialog().dismiss();
                        return true;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        });
        return alertDialogBuilder;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void execute() {
        this.alertDialogBuilder = new AlertDialog.Builder(this.getActivity());
        this.alertDialogBuilder = this.onCreateDialog(this.alertDialogBuilder);
        View view = this.onCreateView();
        if(view != null) {
            this.alertDialogBuilder.setView(view);
        }

        this.onFinish();
        this.onPostViewCreated(view);
    }

    private void onFinish() {
        this.alertDialog = this.alertDialogBuilder.create();
        this.alertDialog.show();
    }

    @Override
    public void onPostViewCreated(View v) {
    }

    @Override
    public View onCreateView() {
        return null;
    }

    private AlertDialog.Builder getDialogBuilder() {
        return this.alertDialogBuilder;
    }

    @Override
    public AlertDialog getDialog() {
        return this.alertDialog;
    }
}