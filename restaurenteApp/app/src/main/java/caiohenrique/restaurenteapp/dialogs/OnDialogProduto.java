package caiohenrique.restaurenteapp.dialogs;

/**
 * Created by Caio on 26/11/2017.
 */

import android.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import caiohenrique.restaurenteapp.Interfaces.Dialog;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.bancodados.Script;
import caiohenrique.restaurenteapp.dialogs.OnDialogBasic;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.requisicoes.Requisicao;

public class OnDialogProduto extends OnDialogBasic {
    private Produto produto;
    private OnDialogProduto.GerenciaProduto gerencia;
    private boolean isEnviadoProduto;

    public OnDialogProduto(Dialog methods) {
        super(methods);
        this.isEnviadoProduto = false;
        this.gerencia = new OnDialogProduto.GerenciaProduto();

    }

    public OnDialogProduto(Dialog methods, Produto produto) {
        this(methods);
        this.produto = produto;
        gerencia.codigo = produto.getId();
    }

    @Override
    public View onCreateView() {
        View view = this.getLayoutInflater().inflate(R.layout.layout_dialog_produtos, (ViewGroup) null);
        this.gerencia.gerenciar(view);
        return view;
    }

    @Override
    public boolean onBackPressed() {
        return this.isEnviadoProduto ? false : super.onBackPressed();
    }


    @Override
    public AlertDialog.Builder onCreateDialog(AlertDialog.Builder alertDialogBuilder) {
        return super.onCreateDialog(alertDialogBuilder);
    }

    public Produto getProduto() {
        return this.produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public OnDialogProduto.GerenciaProduto getGerencia() {
        return this.gerencia;
    }

    public void setGerencia(OnDialogProduto.GerenciaProduto gerencia) {
        this.gerencia = gerencia;
    }

    private void makeText(String text) {
        Toast.makeText(this.getActivity(), text, Toast.LENGTH_LONG).show();
    }

    public class GerenciaProduto {
        private TextView tvCodigo;
        private EditText edtNome;
        private EditText edtQuantidade;
        private EditText edtCusto;
        private EditText edtVenda;
        private Button button;
        private Switch switchLigado;
        private int codigo;

        public int getCodigo() {
            return codigo;
        }

        public GerenciaProduto() {
        }

        void gerenciar(View view) {
            this.recuperarReferencias(view);
            if (OnDialogProduto.this.produto != null) {
                OnDialogProduto.this.produto.setInfos(this);
            }

            this.onListenerEdits();
            this.onClickButton();
        }

        private void onListenerEdits() {
            Script.setListener(this.edtCusto);
            Script.setListener(this.edtVenda);
        }

        private void onClickButton() {
            this.button.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Object object = Produto.notifyClickOfButton(GerenciaProduto.this);
                    if (object instanceof String) {
                        OnDialogProduto.this.makeText((String) object);
                    } else {
                        if (OnDialogProduto.this.produto == null) {

                            OnDialogProduto.this.produto = (Produto) object;
                            GerenciaProduto.this.notifySendNewProduto();
                        } else {
                            OnDialogProduto.this.produto = (Produto) object;

                            GerenciaProduto.this.notifyAlteredProduto();
                        }
                    }
                }
            });
        }

        private void notifyAlteredProduto() {
            OnDialogProduto.this.isEnviadoProduto = true;
            new Thread(new Runnable() {
                @Override
                public void run() {

                    final Produto pt = Requisicao.alterarProduto(produto);

                    methods.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            OnDialogProduto.this.isEnviadoProduto = false;
                            GerenciaProduto.this.button.setEnabled(true);
                            button.setEnabled(false);
                            button.setText("Alterar");
                            if (pt == null) {
                                makeText("Não houve conexão");
                            } else {
                                makeText("Alterado com sucesso!");
                            }
                        }
                    });
                }
            }).start();

        }

        private void notifySendNewProduto() {
            OnDialogProduto.this.isEnviadoProduto = true;
            this.button.setEnabled(false);
            this.button.setText("Enviando");
            (new Thread(new Runnable() {
                public void run() {
                    OnDialogProduto.this.produto = Requisicao.enviarProduto(OnDialogProduto.this.produto);
                    OnDialogProduto.this.getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            OnDialogProduto.this.isEnviadoProduto = false;
                            GerenciaProduto.this.button.setEnabled(true);
                            if (OnDialogProduto.this.produto == null) {
                                GerenciaProduto.this.button.setText("Enviar");
                                OnDialogProduto.this.makeText("Produto não pode ser cadastrado, verifique sua conexão!");
                            } else {
                                GerenciaProduto.this.button.setText("Alterar");
                                OnDialogProduto.this.makeText("Produto cadastrado com sucesso!");
                            }

                        }
                    });
                }
            })).start();
        }

        private void recuperarReferencias(View view) {
            this.tvCodigo = (TextView) view.findViewById(R.id.textviewcodigo);
            this.edtNome = (EditText) view.findViewById(R.id.edittextnome);
            this.edtQuantidade = (EditText) view.findViewById(R.id.edittextquantidade);
            this.switchLigado = (Switch) view.findViewById(R.id.switchv);
            this.edtCusto = (EditText) view.findViewById(R.id.edittextprecocusto);
            this.edtVenda = (EditText) view.findViewById(R.id.edittextprecovenda);
            this.button = (Button) view.findViewById(R.id.button);
        }

        public TextView getTvCodigo() {
            return this.tvCodigo;
        }

        public void setTvCodigo(TextView tvCodigo) {
            this.tvCodigo = tvCodigo;
        }

        public EditText getEdtNome() {
            return this.edtNome;
        }

        public void setEdtNome(EditText edtNome) {
            this.edtNome = edtNome;
        }

        public EditText getEdtQuantidade() {
            return this.edtQuantidade;
        }

        public void setEdtQuantidade(EditText edtQuantidade) {
            this.edtQuantidade = edtQuantidade;
        }

        public EditText getEdtCusto() {
            return this.edtCusto;
        }

        public void setEdtCusto(EditText edtCusto) {
            this.edtCusto = edtCusto;
        }

        public EditText getEdtVenda() {
            return this.edtVenda;
        }

        public void setEdtVenda(EditText edtVenda) {
            this.edtVenda = edtVenda;
        }

        public Button getButton() {
            return this.button;
        }

        public void setButton(Button button) {
            this.button = button;
        }

        public Switch getSwitchLigado() {
            return this.switchLigado;
        }

        public void setSwitchLigado(Switch switchLigado) {
            this.switchLigado = switchLigado;
        }
    }
}
