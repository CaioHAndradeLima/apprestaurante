package caiohenrique.restaurenteapp.dialogs;

/**
 * Created by Caio on 26/11/2017.
 */

import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import caiohenrique.restaurenteapp.Interfaces.Dialog;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.bancodados.BancoManager;
import caiohenrique.restaurenteapp.dialogs.OnDialogBasic;
import caiohenrique.restaurenteapp.objects.Usuario;

public class OnDialogLogin extends OnDialogBasic {
    private EditText editUsuario;
    private EditText editSenha;
    private Button button;
    private Usuario usuario;


    public OnDialogLogin(Dialog methods) {
        super(methods);
    }

    public View onCreateView() {
        View view = this.getLayoutInflater().inflate(R.layout.layout_dialog_login,null);
        this.editUsuario = (EditText)view.findViewById(R.id.edittextlogin);
        this.editSenha = (EditText)view.findViewById(R.id.edittextsenha);
        this.onClickButton((Button)view.findViewById(R.id.button));
        return view;
    }

    @Override
    public android.app.AlertDialog.Builder onCreateDialog(android.app.AlertDialog.Builder builder) {
        return super.onCreateDialog(builder).setTitle("Login");

    }

    private void onClickButton(Button butto) {
        this.button = butto;
        this.button.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                OnDialogLogin.this.button.setEnabled(false);
                OnDialogLogin.this.button.setText("Verificando");
                OnDialogLogin.this.usuario = new Usuario();
                OnDialogLogin.this.usuario.setNome(OnDialogLogin.this.editUsuario.getText().toString());
                OnDialogLogin.this.usuario.setSenha(OnDialogLogin.this.editSenha.getText().toString());
                OnDialogLogin.this.notifySearchLogin();
            }
        });
    }

    private void notifySearchLogin() {
        (new Thread(new Runnable() {
            public void run() {
                OnDialogLogin.this.usuario = OnDialogLogin.this.usuario.verifyNameAndPassword(new BancoManager(OnDialogLogin.this.getActivity()));
                OnDialogLogin.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        if(OnDialogLogin.this.usuario == null) {
                            OnDialogLogin.this.makeText("Não houve conexão");
                        } else if(OnDialogLogin.this.usuario.getPerm() < 1) {
                            OnDialogLogin.this.button.setEnabled(true);
                            OnDialogLogin.this.button.setText("Logar");
                            OnDialogLogin.this.limparEditText();
                            OnDialogLogin.this.makeText(OnDialogLogin.this.usuario.getMessageLoginError());
                        } else {
                            //se tudo deu certo
                            OnDialogLogin.this.onBackPressed();
                        }

                    }
                });
            }
        })).start();
    }

    private void limparEditText() {
        this.editSenha.setText("");
        this.editUsuario.setText("");
    }

    public boolean onBackPressed() {
        Usuario user = (new BancoManager(this.getActivity())).buscarUsuario();
        if(user == null) {
            return true;
        }

        return super.onBackPressed();
    }

    private void makeText(String msg) {
        Toast.makeText(this.getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
