package caiohenrique.restaurenteapp.dialogs;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import caiohenrique.restaurenteapp.ActivityPedido;
import caiohenrique.restaurenteapp.Interfaces;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.bancodados.BancoManager;
import caiohenrique.restaurenteapp.objects.Pedido;
import caiohenrique.restaurenteapp.objects.Produto;
import caiohenrique.restaurenteapp.objects.Usuario;

/**
 * Created by Caio on 27/11/2017.
 */

public class OnDialogAddProduct extends OnDialogBasic implements Interfaces.OnPostProcessamento {

    private TextView tvNome, tvQuantidade;
    private EditText edtConsumidos;
    private Pedido pedido;
    private Produto produto;
    private Button button;
    private boolean isRequisitando = false;

    private int consumidosAntigo = 0;

    public OnDialogAddProduct(Interfaces.Dialog methods, Pedido pedido, Produto produto) {
        super(methods);
        this.pedido = pedido;
        this.produto = produto;

        this.pedido.setListenerOfBeckEnd(this);
    }

    @Override
    public View onCreateView() {
        View view = getLayoutInflater().inflate(R.layout.layout_produto_add_pedido, null);
        recuperarReferencias(view);
        return view;
    }

    private void recuperarReferencias(View view) {
        tvNome = view.findViewById(R.id.textview_nome_);
        tvQuantidade = view.findViewById(R.id.textview_quantidade_);
        edtConsumidos = view.findViewById(R.id.edittextquantidade);
        button = view.findViewById(R.id.button);
    }

    @Override
    public void onPostViewCreated(View v) {
        setDadosInLayout();
        onClickButton();
        super.onPostViewCreated(v);
    }

    private void setDadosInLayout() {
        tvNome.setText(produto.getNome());
        tvQuantidade.setText(produto.getQuantidade() + "");

        consumidosAntigo = pedido.getConsumidos(produto);


        if (consumidosAntigo == 0) {
            button.setText("inserir");
            edtConsumidos.setText("0");
        } else {
            button.setText("Alterar");
            edtConsumidos.setText(Integer.toString(consumidosAntigo));
        }
    }

    private void onClickButton() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPostClickButton();
            }
        });
    }

    public void onPostClickButton() {
        if(validate()) {
            button.setText("Aguarde");
            button.setEnabled(false);
            edtConsumidos.setEnabled(false);
            isRequisitando = true;
        }
    }

    /***********
     **A ideia aqui é enviar a acao apenas de modificacao
     * vamos pegar o que aconteceu, se o numero diminuiu, crescer, ou nao foi modificado
     * return true se ele foi fazer requisicao http
     */
    private boolean validate() {

        int quantAtual = Integer.parseInt(edtConsumidos.getText().toString());
        if (quantAtual == consumidosAntigo) {
            makeText("Não foi alterada a quantidade de consumidos");
        } else {
            Usuario usuario = new BancoManager(getActivity()).buscarUsuario();

            if (quantAtual > consumidosAntigo) {
                pedido.notifyChangeQuantProduct(produto, usuario, Integer.parseInt(edtConsumidos.getText().toString()));
                return true;
            } else {
                //para retirar um produto é necessário a permissão
                //regra de negocio da empresa
                if (usuario.getPerm() == 0) {
                    makeText("Não há permissão para diminuir a quantidade!");
                } else {
                    pedido.notifyChangeQuantProduct(produto, usuario, Integer.parseInt( edtConsumidos.getText().toString() ));
                    return true;
                }
            }
        }
        return false;
    }

    private void makeText(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onBackPressed() {

        if(isRequisitando) return true;

        return super.onBackPressed();
    }


    @Override
    public void onSave(boolean success, String msg) {
        //nada acontece
        //nao pedimos pra salvar o pedido nesta tela
    }

    @Override
    public void onAltered(boolean success,final String msg) {
        //chamado quando o pedido for alterado, se houve conection return true
        //se nao, retorna a msg de erro
        isRequisitando = false;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (msg != null)
                    makeText(msg);
                else {
                    makeText("Alterado com sucesso!");
                    button.setText("Alterado!");
                    onBackPressed();
                    ( (ActivityPedido) methods.getActivity()).notifyAlteredList();
                }
            }
        });
    }
}
