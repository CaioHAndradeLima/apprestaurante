package caiohenrique.restaurenteapp.dialogs;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import caiohenrique.restaurenteapp.ActivityPedido;
import caiohenrique.restaurenteapp.Interfaces;
import caiohenrique.restaurenteapp.R;
import caiohenrique.restaurenteapp.bancodados.Script;
import caiohenrique.restaurenteapp.objects.Payment;
import caiohenrique.restaurenteapp.objects.Produto;

/**
 * Created by Caio on 29/11/2017.
 */

public class OnDialogPayment extends OnDialogBasic {

    private RecyclerView list;
    private EditText edtValor;
    private Button button;
    private Payment payment;

    public OnDialogPayment(Interfaces.Dialog methods, int idPedido) {
        super(methods);
        payment = new Payment();
        //em estado enviamos o id do pedido...
        payment.setEstado(idPedido);
    }


    @Override
    public View onCreateView() {
        return getLayoutInflater().inflate(R.layout.layout_dialog_add_payment, null);
    }

    @Override
    public void onPostViewCreated(View v) {
        recuperarReferencias(v);
        admListView();
        admEdt();
        onClick();

        super.onPostViewCreated(v);
    }

    private void onClick() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment.setValor(edtValor.getText().toString());
                String validate = payment.validate();
                if (validate != null) {
                    makeText(validate);
                } else {
                    notifyOnClick();
                }
            }
        });
    }

    private void notifyOnClick() {
        button.setEnabled(false);
        new Thread(new Runnable() {
            @Override
            public void run() {

                Payment p;
                if (payment.getId() == 0)
                    p = payment.notifyInserded();
                else
                    p = payment.notifyAltered();

                onPostRequisicao(p);
            }
        }).start();

    }

    private void onPostRequisicao(final Payment p) {

        methods.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (p == null) {
                    makeText("Não houve conexão!");
                    button.setEnabled(true);
                } else {
                    makeText("Pedido alterado!");
                    ((ActivityPedido) methods.getActivity()).notifyAlteredList();
                }

            }
        });
    }

    private void makeText(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private void admEdt() {
        Script.setListener(edtValor);
    }

    private void recuperarReferencias(View v) {
        edtValor = v.findViewById(R.id.edittextvalor);
        list = v.findViewById(R.id.recyclerview);
        button = v.findViewById(R.id.button);
    }

    private void admListView() {

        Adapter adapter = new Adapter();

        list.setLayoutManager(new LinearLayoutManager(methods.getActivity()));
        list.setAdapter(adapter);

        //necessario por causa do list dentro do scrollview
        /*
        list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        */
    }


    private class Adapter extends RecyclerView.Adapter {

        List<String> lista = new ArrayList<>();

        public Adapter() {
            String[] lista = methods.getActivity().getResources().getStringArray(R.array.array_tipos);
            for (String itemLista : lista) {
                this.lista.add(itemLista);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new HolderList(methods.getLayoutInflater().inflate(R.layout.layout_lista_tipos_payment, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            HolderList holderList = (HolderList) holder;
            holderList.textview.setText(lista.get(position));
            if (!payment.getTipo().equals(lista.get(position)))
                holderList.container.setBackgroundColor(methods.getActivity().getResources().getColor(R.color.branca));
        }

        @Override
        public int getItemCount() {
            return lista.size();
        }

        private String getTipo(int adapterPosition) {
            return lista.get(adapterPosition);
        }

        private class HolderList extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView textview;
            private LinearLayout container;

            public HolderList(View itemView) {
                super(itemView);
                textview = itemView.findViewById(R.id.textview);
                container = itemView.findViewById(R.id.linearlayout);
                container.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (getAdapterPosition() == -1) return;


                payment.setTipo(getTipo(getAdapterPosition()));
                notifyDataSetChanged();
                container.setBackgroundColor(methods.getActivity().getResources().getColor(R.color.colorclicked));

            }
        }
    }


}
