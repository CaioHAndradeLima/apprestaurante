package caiohenrique.restaurenteapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import caiohenrique.restaurenteapp.objects.Produto;

/**
 * Created by Caio on 26/11/2017.
 */

public interface Interfaces {

    public interface CycleLifeDialog {
        AlertDialog.Builder onCreateDialog(AlertDialog.Builder builder);

        boolean onBackPressed();

        void onPostViewCreated(View var1);

        AlertDialog getDialog();

        View onCreateView();
    }

    interface onClickHolder {
        void onClick(int position);
    }
    public interface Holders {
        void recuperarReferencias();
        void bindViewHolder(int position, Produto produto);
    }
    public interface Dialog {
        Activity getActivity();

        LayoutInflater getLayoutInflater();
    }

    interface OnPostProcessamento {
        void onSave(boolean success, String mensagem);
        void onAltered(boolean success,String mensagem);
    }

    interface OnAlteredProduto {
        void onAltered(Produto produto);
    }
}
